try:
    import pygame
    from pygame.locals import*
except:
    print("Probleme d'importation pygame. Si vous avez choisi de ne pas activer l'interface graphique pendant la partie lors de l'appel du constructeur du serveur, cela ne posera pas de probleme.")
from tkinter import*
from math import*

#pour pouvoir recuperer la taille de l'ecran sous windows(ctype) ou linux (subprocess)
import platform
import ctypes
import subprocess

class Interface:
     
    def __init__(self, server, display_game):
        """ constructeur de la classe """
        
        self.server = server
       
        self.display_game = display_game 
        self.pause = True

        #initialisation des dimensions de l'ecran
        if platform.system() == "Linux":
            self.set_screen_size_linux()
        else:
            self.set_screen_size_windows()
            self.window_size_x -= 50
            self.window_size_y -= 80

        if self.display_game:
            #creation de la fenetre et des images
            pygame.init() 
                    
            self.window = pygame.display.set_mode((self.window_size_x, self.window_size_y))
            self.creation_images()
            self.font = pygame.font.Font(None, 30)


    def init_game(self):
        """ reinitialise la partie """
        self.current_player = self.server.current_pid  #joueur courant
        self.last_looser = -1  #dernier joueur a avoir perdu (-1 signifie personne). si initialise reste en place tant que le joueur ne change pas.

        #initialisation des liste d'infos sur les joueurs
        self.players_alive = [True]*self.nb_players
        self.players_dices = [[0,0,0,0,0,0]]*self.nb_players
        self.players_choice = [(-2,0)]*self.nb_players
        self.players_name = [""]*self.nb_players
        for i in range(self.nb_players):
            self.players_name[i] = self.server.players[i].name



    def set_screen_size_linux(self):
        """ recupere les dimensions de l'ecran, appelee si l'os est linux """
        process = subprocess.Popen("xrandr  | grep \* | cut -d' ' -f4", stdout=subprocess.PIPE, shell=True)
        str_dim = process.communicate()[0]
        str_dim = str(str_dim)[2:len(str(str_dim))-3]
        axe = 'x'
        str_dim_x = ""
        str_dim_y = ""
        for c in str_dim:
            if c != 'x':
                if axe == 'x':
                    str_dim_x += c
                else:
                    str_dim_y += c
            else:
                axe = 'y'
        self.window_size_x = int(str_dim_x)
        self.window_size_y = int(str_dim_y)



    def set_screen_size_windows(self):
        """ recupere les dimension de l'ecran, appelee si l'os n'est pas linux """
        user32 = ctypes.windll.user32
        self.window_size_x = user32.GetSystemMetrics(0)
        self.window_size_y = user32.GetSystemMetrics(1)
 
        

    def init_serie_game(self, nb_players):
        """ initialise une serie de parties """
        self.nb_players = nb_players
        self.players_nb_victory = [0]*self.nb_players
        self.creation_img_color_masks()
        
        #initialisation des indications qui serviront pour le placement des images
        self.nb_column_cups = 5
        self.nb_column_dices = 2
        self.space_x = self.window_size_x/self.nb_column_cups
        self.space_y = self.window_size_y/ceil(self.nb_players/self.nb_column_cups)


    
    def create_color(self, pid):
        """ renvoie un tiplet (R,V,B) correspondant a la couleur du joueur d'indice "pid" """        

        space_between_colors = (255*2*3)//self.nb_players
        n = space_between_colors*pid
      
        #on part de la couleur (R,V,B) = (255,0,0) (rouge),
        #et on parcours le cercle chromatique selon la "distance" necessaire pour atteindre la couleur voulue
        #on commence par "monter" la composante verte pour "glisser" vers le jaune,
        #puis on "descendra" le rouge pour glisser vers le vert, etc
        color = [255,0,0]
        etat = 1
        i = 1
        while n > 0:
            
            if n <= 255:
                if etat == 1:
                    color[i] += n
                else:
                    color[i] -= n
                n = 0
            else:
                n -= 255
                if etat == 1:
                    color[i] = 255
                    etat = 0
                else:
                    color[i] = 0
                    etat = 1
                if i == 0:
                    i = 2
                else:
                    i -= 1
            n -= 1
        
        return Color(color[0], color[1], color[2])



    def replace_color(self, img, original_color, final_color):
        """ remplace original_color par final_color dans l'image img """
        img_temp = pygame.PixelArray(img)  
        img_temp.replace(original_color, final_color)
        img.unlock()
      
  
    
    def creation_images(self):
        """ charge les images depuis les fichiers .bmp """

        #chargement des images
        self.background = pygame.Surface((self.window_size_x, self.window_size_y))
        self.dice_img = pygame.image.load("Images/dice.bmp").convert()
        self.dice_1_img = pygame.image.load("Images/dice_1.bmp").convert()
        self.dice_2_img = pygame.image.load("Images/dice_2.bmp").convert()
        self.dice_3_img = pygame.image.load("Images/dice_3.bmp").convert()
        self.dice_4_img = pygame.image.load("Images/dice_4.bmp").convert()
        self.dice_5_img = pygame.image.load("Images/dice_5.bmp").convert()
        self.dice_6_img = pygame.image.load("Images/dice_6.bmp").convert()
        self.speech_bubble_img = pygame.image.load("Images/speech_bubble.bmp").convert()
        self.cup_img = pygame.image.load("Images/cup.bmp").convert()
        self.skull_img = pygame.image.load("Images/skull.bmp").convert()
        self.info_board_img = pygame.image.load("Images/info_board.bmp").convert()
        
        #on rend la couleur blanche transparente pour certaines images
        self.background.fill((220, 220, 220))
        self.cup_img.set_colorkey((255,255,255))
        self.dice_1_img.set_colorkey((255,255,255))
        self.dice_2_img.set_colorkey((255,255,255))
        self.dice_3_img.set_colorkey((255,255,255))
        self.dice_4_img.set_colorkey((255,255,255))
        self.dice_5_img.set_colorkey((255,255,255))
        self.dice_6_img.set_colorkey((255,255,255))
        self.speech_bubble_img.set_colorkey((255,255,255))
        self.skull_img.set_colorkey((255,255,255))
        self.info_board_img.set_colorkey((255,255,255))



    def creation_img_color_masks(self): 
        #creation des listes de masques de couleur pour les des, les gobelets et le panneau d'informations
        self.players_cup_color_img = [None]*self.nb_players
        self.players_dice_color_img = [None]*self.nb_players
        for i in range(self.nb_players):
            self.players_cup_color_img[i] = pygame.image.load("Images/cup_mask.bmp").convert()
            self.players_dice_color_img[i] = pygame.image.load("Images/dice_mask.bmp").convert()
            self.players_cup_color_img[i].set_colorkey((255,255,255))
            self.players_dice_color_img[i].set_colorkey((0,0,0))
            
            self.replace_color(self.players_cup_color_img[i], Color(195,195,195), self.create_color(i))
            self.replace_color(self.players_dice_color_img[i], Color(255,255,255), self.create_color(i))
        
        self.info_board_yellow_img = pygame.image.load("Images/info_board_mask.bmp").convert()
        self.info_board_red_img = pygame.image.load("Images/info_board_mask.bmp").convert()
        self.info_board_yellow_img.set_colorkey((255,255,255))
        self.info_board_red_img.set_colorkey((255,255,255))
        self.replace_color(self.info_board_yellow_img, Color(0,0,0), Color(255,242,0))
        self.replace_color(self.info_board_red_img, Color(0,0,0), Color(237,28,36))
        
           
 
    def display(self, round_end = False):
        """ affiche la partie en utilisant une interface graphique """
        
        #on actualise les informations de la partie
        self.update_infos()
 
        if self.display_game: 
            
            if self.current_player != self.server.current_pid:
                self.current_player = self.server.current_pid
                self.last_looser = -1   #une nouvelle  manche a du redemarrer, on n'affiche plus le dernier perdant
    
            #on ecrase l'affichage precedent en affichant un fond de la taille de la fenetre
            self.window.blit(self.background, (0, 0))
        
            #on colle les image des elements de la partie (des, gobelets, choix, panneau d'information)
            if round_end:
                self.blit_dices()
            else:
                self.blit_cups()
                self.blit_choices()
            self.blit_info_board()
             
            #on actualise l'ecran
            pygame.display.flip()

            #si pause, on attend un appui sur une touche du clavier (et appui sur p -> pause = False)
            if self.pause:
                self.wait_keydown()
            #sinon, si appui sur p -> pause=True 
            else:
                self.set_pause()



    def wait_keydown(self):
        """ attente d'un appui touche clavier, defait la pause si appui sur p """
        end = False
        while not end:
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_p:
                        self.pause = False
                    end = True



    def set_pause(self):
        for event in pygame.event.get():
                if event.type == KEYDOWN and event.key == K_p:
                        self.pause = True

                        
 
    def update_infos(self):
        """ actualise les infos sur les joueurs a partir du pointeur vers le serveur """
        for i in range(self.nb_players):
            self.players_alive[i] = self.server.players[i].alive
            self.players_dices[i] = self.server.players[i].dices
            self.players_choice[i] = self.server.players[i].choice

        #s'il y a eu une nouvelle victoire on l'ajoute aux stats de l'IA en question
        d = self.new_victory()
        if d != -1:
            self.players_nb_victory[d] += 1



    def new_victory(self):
        """ renvoie l'indice du joueur qui vient de gagner s'il y en a un, ou -1 sinon"""
        nb_dead = 0
        for i in range(self.nb_players):
            if self.players_alive[i]:
                pid = i
            else:
                nb_dead += 1
        if nb_dead == self.nb_players-1:
            return pid
        else:
            return -1


 
    def blit_cups(self):
        """ affiche les gobelets de tous les joueurs s'ils sont vivants ou un crane sinon """
        pos_x = 0
        pos_y = 0
        
        for i in range(self.nb_players):
            
            if self.players_alive[i]:
                self.window.blit(self.cup_img, (pos_x, pos_y))
                self.window.blit(self.players_cup_color_img[i], (pos_x, pos_y))
            else:
                self.window.blit(self.skull_img, (pos_x, pos_y))
            
            pos_x += self.space_x
            if pos_x >= self.space_x*self.nb_column_cups:
                pos_x = 0
                pos_y += self.space_y
                
                 
    
    def blit_dices(self):
        """ affiche les des de tous les joueurs s'ils sont vivants ou un crane sinon,
        et met en evidence les des concernes par le dernier pari"""
        decalage_des = (self.cup_img.get_width()-self.dice_img.get_width()*self.nb_column_dices)/2
        pos_cup_x = decalage_des
        pos_cup_y = 0
        betted_dice_value = self.players_choice[self.precedent(self.current_player)][1]

        for i in range(self.nb_players):
            
            if self.players_alive[i]:
                pos_dice_x = pos_cup_x
                pos_dice_y = pos_cup_y + self.cup_img.get_height() - self.dice_img.get_height()
                
                for k in range(0,len(self.players_dices[i])):
                    
                    if betted_dice_value == self.players_dices[i][k]: 
                        self.replace_color(self.dice_img, Color(0,0,0), Color(255,255,255))
                        self.window.blit(self.dice_img, (pos_dice_x, pos_dice_y))
                        self.replace_color(self.dice_img, Color(255,255,255), Color(0,0,0))
                    else:
                        self.window.blit(self.dice_img, (pos_dice_x, pos_dice_y))

                    self.window.blit(self.players_dice_color_img[i], (pos_dice_x, pos_dice_y))
                    dice = self.players_dices[i][k]
                    if dice == 1:
                        self.window.blit(self.dice_1_img, (pos_dice_x, pos_dice_y))
                    elif dice == 2:
                        self.window.blit(self.dice_2_img, (pos_dice_x, pos_dice_y))
                    elif dice == 3:
                        self.window.blit(self.dice_3_img, (pos_dice_x, pos_dice_y))
                    elif dice == 4:
                        self.window.blit(self.dice_4_img, (pos_dice_x, pos_dice_y))
                    elif dice == 5:
                        self.window.blit(self.dice_5_img, (pos_dice_x, pos_dice_y))
                    elif dice == 6:
                        self.window.blit(self.dice_6_img, (pos_dice_x, pos_dice_y))
                    
                    pos_dice_x += self.dice_img.get_width()
                    if pos_dice_x >= pos_cup_x + self.dice_img.get_width()*self.nb_column_dices:
                        pos_dice_x = pos_cup_x
                        pos_dice_y -= self.dice_img.get_height()
            else:
                self.window.blit(self.skull_img, (pos_cup_x-decalage_des, pos_cup_y))
            
            pos_cup_x += self.space_x
            if pos_cup_x >= self.space_x*self.nb_column_cups:
                pos_cup_x = decalage_des
                pos_cup_y += self.space_y
                
                
     
    def blit_choices(self):
        """ affiche les choix des joueurs """
        pos_x = 0
        pos_y = 0
        
        for i in range(self.nb_players):
            
            if self.players_alive[i]:
                
                choice = self.players_choice[i]
                if choice[0] == -1:
                    text = self.font.render("DUDO !", 1, (10, 10, 10))
                elif choice[0] == 0:
                    text = self.font.render("CALZA !", 1, (10, 10, 10))
                elif not (choice[0] == 1 and choice[1] == 0):
                    if choice[1] == 1:
                        text = self.font.render(str(choice[0]) + " pacos", 1, (10, 10, 10))
                    else:
                        text = self.font.render(str(choice[0]) + " " + str(choice[1]), 1, (10, 10, 10))
                    
                if not (choice[0] == 1 and choice[1] == 0):
                    self.window.blit(self.speech_bubble_img, (pos_x, pos_y))
                    self.window.blit(text, (pos_x + 52, pos_y + 205))
            
            pos_x += self.space_x
            if pos_x >= self.space_x*self.nb_column_cups:
                pos_x = 0
                pos_y += self.space_y
                
                
     
    def blit_info_board(self):
        """ affiche un petit panneau d'information sous chaque joueur """
        pos_x = 0
        pos_y = self.cup_img.get_height()
        
        for i in range(self.nb_players):
            
            #affichage du panneau d'information du joueur i
            self.window.blit(self.info_board_img, (pos_x, pos_y))
            if i == self.current_player:
                self.window.blit(self.info_board_yellow_img, (pos_x, pos_y))
            if i == self.last_looser:
                self.window.blit(self.info_board_red_img, (pos_x, pos_y))
            
            #affichage du nom de l'ia et des des qu'elle a dans le panneau d'informations
            text = self.font.render(self.players_name[i] + " : ", 1, (10, 10, 10))
            self.window.blit(text, (pos_x+30, pos_y+4))
            if self.players_alive[i]:
                str_dices_list = "Dés : "
                for k in range(len(self.players_dices[i])):
                    str_dices_list += str(self.players_dices[i][k]) + " "
                text = self.font.render(str_dices_list, 1, (10, 10, 10))
            else:
                text = self.font.render("MORT", 1, (10, 10, 10))
            self.window.blit(text, (pos_x+30, pos_y+26))
            
            pos_x += self.space_x
            if pos_x >= self.space_x*self.nb_column_cups:
                pos_x = 0
                pos_y += self.space_y
                

 
    def display_console(self, round_end = False):
        """ afiche la partie en utilisant la console """
        for i in range(self.nb_players):
            
            print("Joueur ", i, " : ", end="")

            if self.players_alive[i]:
                if round_end:
                    print("Des : ", end="")
                    for k in range(len(self.players_dices[i])):
                        print(self.players_dices[i][k], end = " ")
                else:
                    if self.players_alive[i]:
                                    
                        if self.players_choice[i][0] != -2:
                            if self.players_choice[i][0] == -1:
                                print("DUDO !")
                            elif self.players_choice[i][0] == 0:
                                print("CALZA !")
                            else:
                                print(self.players_choice[i][0], self.players_choice[i][1])
                        else:
                            print("--attend son tour--")
            else:
                print("mort")
        print("")



    def display_results(self):
        #on quite pygame
        if self.display_game:
            pygame.quit()
        
        #creation de la fenetre tkinter
        self.window = Tk()        
        self.window.geometry(str(self.window_size_x)+"x"+str(self.window_size_y)+"+0+0")
        self.window.bind('<Escape>', lambda e : self.window.destroy())
        
        #generation des lignes de texte correspondant aux resultats de chaque joueur
        self.players_result = [""]*self.nb_players
        self.create_text()
        for i in range(self.nb_players):
            self.players_result[i].pack()

        self.window.mainloop()
        print("le programme s'est termine correctement")
        


    def create_text(self):
        """ cree toutes les lignes de texte correspondant aux resultats de chaque joueur """
        for i in range(self.nb_players):
            str_result = self.players_name[i] + " : "
            l_name = len(str_result)+3
            str_result += "nombre de victoires : " + str(self.players_nb_victory[i]) + '\n'
            str_result += l_name*" " + "pourcentages : \n"
            str_result += l_name*" " + "calza : ?\n"
            str_result += l_name*" " + "dudo : ?\n\n"
            self.players_result[i] = Label(self.window, text=str_result, justify=LEFT, font=("Helvetica", 16))



    def precedent(self, pid):
        """ renvoie l'indice du joueur precedent """
        if pid > 0:
            return pid-1
        else:
            return self.nb_players-1

        
        
    def set_looser(self, pid):
        """ enregistre l'identifiant du perdant donne en parametre """
        self.last_looser = pid
