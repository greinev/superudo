import os
from superudo import Server
import game_IA
from importlib import import_module
from interface import *

IA_list = []

for file in os.listdir("game_IA"):
    if file[-3:]==".py":
        try:
            module = import_module("game_IA."+file[:-3], "game_IA")
            loaded_IA = module.IA
            IA_list.append(loaded_IA)
        except:
            print("Error while importing", file[:-3])
        else:
            print("Importing", file[:-3])

if IA_list==[]:
    try:
        file = "default_IA.py"
        module = import_module("IA."+file[:-3], "game_IA")
        loaded_IA = module.IA
        IA_list = [loaded_IA]*3
    except:
        raise
        #print("Error while importing", file[:-3])
    #else:
        #print("Importing", file[:-3])

s = Server(UI=Interface, text_output=True, display_game=True)
s.play(IA_list, 50)
