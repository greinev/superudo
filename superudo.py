import os
import game_IA
from importlib import import_module
from random import seed, randint

class Server():
    def __init__(self, UI=None, text_output=True, display_game=True):
        if UI:
            self.UI = UI(self, display_game)
        else:
            self.UI = None
        self.text_output = text_output 



    def init_game(self, clients):
        self.current_pid = 0
        self.palifico = False
        self.palifico_next_turn = False
        self.players_number = self.initial_players_number
        for i in range(self.players_number):
            self.players[i] = Player(self, self.initial_players_number, clients[i], i)




    def play(self, clients, n):
        #initialisation de la serie de games
        self.players = {}
        self.initial_players_number = len(clients)
        for i in range(self.initial_players_number):
            self.players[i] = Player(self, self.initial_players_number, clients[i], i)
        if self.UI:
            self.UI.init_serie_game(len(clients))

        #on effectue les n games
        for i in range(n):
            self.init_game(clients)
            if self.UI:
                self.UI.init_game()
            self.game()
            print("partie ", i, " terminee")

        #on affiche les resultats de cette serie de game
        if self.UI:
            self.UI.display_results()
        


    def game(self):
        while self.players_number>1:
            self.new_round()
            if self.palifico_next_turn:
                self.palifico_next_turn = False
                self.palifico = True
                self.broadcast_set_palifico()
                if self.text_output:
                    print("PALIFICO !!!")
            else:
                self.palifico = False
            if not self.players[self.current_pid].is_alive():
                self.current_pid = self.get_next_pid(self.current_pid)
            for player in self.players.values():
                if player.is_alive():
                    dices = []
                    for i in range(player.get_dices_number()):
                        dices.append(randint(1, 6))
                    player.set_dices(dices[:])
            if self.UI:
                self.UI.display()
            old_pid = -1
            old_choice = (1, 0)
            self.choice = (1, 0)
            while self.choice[0]!=-1 and self.choice[0]!=0 and self.choice[0]!=-2:
                self.choice = self.players[self.current_pid].ask()
                if self.text_output:
                    print(self.players[self.current_pid].get_name(), "has chosen", self.choice)
                if not self.choice_is_legit(self.choice, old_choice, old_pid) :
                    if old_pid==-1:
                        if self.text_output:
                            print("Choice not legit : replacing it with Forfeit")
                            #print(old_pid, old_choice)
                        self.choice = (-2, 0)
                    else:
                        if self.text_output:
                            print("Choice not legit : replacing it with Dudo")
                        self.choice = (-1, 0)
                self.players[self.current_pid].choice = self.choice
                self.broadcast_player_choice(player, self.choice)
                if self.choice[0]!=-1 and self.choice[0]!=0 and self.choice[0]!=-2:
                    old_choice = self.choice[:]
                    old_pid = self.current_pid
                    self.current_pid = self.get_next_pid(self.current_pid)
                else:
                    pass
                if self.UI:
                    self.UI.display()
            if self.choice[0]==-2:
                # Ragequit
                if self.text_output:
                    print("Forfeit !")
                self.remove_player_dice(self.players[self.current_pid])
            elif self.choice[0]==-1:
                # Dudo
                if self.text_output:
                    print("Dudo !")
                if self.dudo_is_correct(old_choice, self.palifico):
                    self.remove_player_dice(self.players[old_pid])
                    if self.text_output:
                        print("-> Correct !")
                else:
                    if self.text_output:
                        if self.calza_is_correct(old_choice, self.palifico):
                            print("-> Eh non, il fallait dire calza !")
                        else:
                            print("-> Perdu !")
                    self.remove_player_dice(self.players[self.current_pid])
            elif self.choice[0]==0:
                # Calza
                if self.text_output:
                    print("Calza !!!")
                if self.calza_is_correct(old_choice, self.palifico):
                    if self.text_output:
                        print("-> GG !")
                    self.add_player_dice(self.players[self.current_pid])
                else:
                    if self.text_output:
                        print("-> Raté !")
                    self.remove_player_dice(self.players[self.current_pid])
                pass
            else:
                print("Fatal Error.")
            if self.UI:
                self.UI.display(True)
                    
        if self.text_output:
            print("Fin de la partie.")
        winning_pid = -1
        for pid in self.players:
            if self.players[pid].is_alive():
                winning_pid = pid

        return winning_pid
    #print(self.players)

    # Natives

    def new_round(self):
        if self.text_output:
            print("New round")
        for pid in self.players:
            self.players[pid].choice = (1, 0)
            self.players[pid].send_new_round()

    def remove_player_dice(self, player):
        player.remove_dice()
        for pid in self.players:
            self.players[pid].send_remove_player_dice(player)
        if player.get_dices_number()==1:
            self.palifico_next_turn = True
        elif player.get_dices_number()==0:
            player.lose()
            self.players_number -= 1
        if self.UI:
            self.UI.set_looser(player.pid)

    def add_player_dice(self, player):
        player.add_dice()
        for pid in self.players:
            self.players[pid].send_add_player_dice(player)

    def show_all_dices(self):
        for pid_1 in self.players:
            for pid_2 in self.players:
                self.players[pid_1].set_player_dices(pid_2, self)

    def broadcast_player_choice(self, player, choice):
        for player_2 in self.players.values():
            player_2.send_player_choice(player, choice)

    def broadcast_set_palifico(self):
        for pid in self.players:
            self.players[pid].send_set_palifico()

    def choice_is_legit(self, choice, last_choice, last_pid):
        if choice[0]==0 or choice[0]==-1 or choice[0]==-2:
            return last_pid!=-1
        elif choice[0]<0 or choice[1] not in (1, 2, 3, 4, 5, 6):
            return False
        elif last_choice[0]==1 and last_choice[1]==0:
            return True
        elif choice[1]==last_choice[1]:
            # meme elem (paco -> paco)
            return choice[0]>last_choice[0]
        elif self.palifico:
            return False
        elif choice[1]==1:
            # normal -> paco
            return choice[0]>=last_choice[0]/2
        elif last_choice[1]==1:
            # paco -> normal
            return choice[0]>=2*last_choice[0]+1
        else:
            # normal -> normal different
            return choice[0]>last_choice[0] or (choice[0]==last_choice[0] and choice[1]>last_choice[1])

    def dudo_is_correct(self, last_choice, palifico):
        return self.get_x_number(last_choice[1], palifico)<last_choice[0]

    def calza_is_correct(self, last_choice, palifico):
        return self.get_x_number(last_choice[1], palifico)==last_choice[0]

    def get_x_number(self, x, palifico):
        total = 0
        for player in self.players.values():
            if player.is_alive():
                for dice in player.get_dices():
                    if dice==x or ((not palifico) and dice==1):
                        total += 1
        #print("il y a", total, x, palifico)
        return total

    def get_next_pid(self, pid):
        if len(self.players)>=1:
            next_pid = (pid+1)%self.initial_players_number
            while not self.players[next_pid].is_alive():
                next_pid = (next_pid+1)%self.initial_players_number
            return next_pid
        else:
            raise

    def get_text_output(self):
        return self.text_output

class Player():
    def __init__(self, server, players_number, client, pid):
        self.server = server
        self.client = client(players_number, pid, self.server.get_text_output())
        self.pid = pid
        self.name = self.client.get_name()
        self.dices_number = 5
        self.dices = [0]*self.dices_number
        self.alive = True

    def get_name(self):
        return self.name

    def lose(self):
        if self.server.get_text_output():
            print(self.name, "a PERDU")
        self.alive = False

    def is_alive(self):
        return self.alive

    def get_dices(self):
        return self.dices

    def get_dices_number(self):
        return self.dices_number

    def set_dices(self, dices):
        self.dices = dices[:]
        self.client.set_dices(self.dices[:])
        if self.server.get_text_output():
            print("The dices of", self.name, "are now", self.dices)

    def set_dices_number(self, n):
        self.dices_number = n
        if self.server.get_text_output():
            print("The dices number of", self.name, "is now", self.dices_number)

    def remove_dice(self):
        self.dices_number -= 1
        if self.server.get_text_output():
            print("The dices number of", self.name, "is now", self.dices_number)

    def add_dice(self):
        self.dices_number += 1
        if self.server.get_text_output():
            print("The dices number of", self.name, "is now", self.dices_number)

    def send_new_round(self):
        self.client.new_round()

    def send_remove_player_dice(self, player):
        self.client.remove_player_dice(player.pid)

    def send_add_player_dice(self, player):
        self.client.add_player_dice(player.pid)

    def send_player_dices(self, player):
        self.client.set_player_dice(player.pid, player.get_dices()[:])

    def send_player_choice(self, player, choice):
        self.client.set_player_choice(player.pid, choice[:])

    def send_set_palifico(self):
        self.client.set_palifico()

    def ask(self):
        return self.client.play()

#IA_list = []
#
#for file in os.listdir("game_IA"):
#    if file[-3:]==".py":
#        try:
#            module = import_module("game_IA."+file[:-3], "game_IA")
#            loaded_IA = module.IA
#            IA_list.append(loaded_IA)
#        except:
#            print("Error while importing", file[:-3])
#        else:
#            print("Importing", file[:-3])
#
#if IA_list==[]:
#    try:
#        file = "default_IA.py"
#        module = import_module("IA."+file[:-3], "game_IA")
#        loaded_IA = module.IA
#        IA_list = [loaded_IA]*3
#    except:
#        raise
#        #print("Error while importing", file[:-3])
#    else:
#        print("Importing", file[:-3])
#
#
#
#s = Server(IA_list)
#
#s.UI = Interface(s)
#
#s.game()
