# default_IA.py by Victor Greiner
# This is an exemple of an IA file for superudo.
# You can copy and edit this if you want to create your own IA.
# The file name does not matter but the class name (IA) must remain the same
# and certain of it's methods must exist.

from random import randint, choice

class IA():

    # Interface methods 
    # These are the methods called by the server when a specific event occurs
    # or when an information about the IA is required
    # You can edit these methods but not remove them
    # (this would make the server crash)

    def __init__(self, player_number, pid, text_output):
        """This method is called upon game start."""
        self.player_number = player_number
        self.pid = pid
        self.text_output = text_output
        self.name = ""
        voyelles = "aeiouyaeiou"
        consonnes = "zrtpqsdfghjklmwxcvbnrtpqsdfghjklmcvbn"
        if randint(0, 1):
            self.name += choice(voyelles.upper())
            self.name += choice(consonnes) + choice(voyelles)
            if randint(0, 3):
                self.name += choice(consonnes) + choice(voyelles)
            elif randint(0, 3):
                self.name += choice(consonnes)
        else:
            self.name += choice(consonnes.upper())
            self.name += choice(voyelles) + choice(consonnes)
            if randint(0, 3):
                self.name += choice(voyelles) + choice(consonnes)
            elif randint(0, 3):
                self.name += choice(voyelles)
        self.choices = {}
        self.dices_number = {}
        self.dices = {}
        self.palifico = False
        self.last_choice = (1, 0)
        for i in range(player_number):
            self.choices[i] = None
            self.dices_number[i] = 5
            self.dices[i] = []

    def get_name(self):
        """Return the name of the player"""
        return self.name

    def new_round(self):
        """This method is called upon each round start."""
        self.palifico = False
        self.last_choice = (1, 0)
        for pid in self.choices:
            self.choices[pid] = (1, 0)
            self.dices[pid] = []

    def set_dices(self, dices):
        """This method is called right after the round start
        to let the player know what his dices are."""
        self.dices[self.pid] = dices

    def set_player_dices(self, pid, dices):
        """This method is called at the end of each round
        to let the player know what other player's dices are."""
        self.dices[pid] = dices

    def set_player_choice(self, pid, choice):
        """This method is called whenever a player makes a bet."""
        self.choices[pid] =  choice
        self.last_choice = choice[:]

    def remove_player_dice(self, pid):
        """This method is called whenever a player loses a dice."""
        self.dices_number[pid] -= 1

    def add_player_dice(self, pid):
        """This method is called whenever a player gains a dice."""
        self.dices_number[pid] += 1

    def set_palifico(self):
        """This method is called right after the beginning of a round with Palifico."""
        self.palifico = True

    def play(self):
        """This method is called when this player has to make a bet.
        It must return a couble of int that represent a bet.
        Examples :
            (-1, 0) -> Dudo
            (0, 0) -> Calza
            (2, 1) -> 2 Paco
            (3, 5) -> 3 Dices with a value of 5
            (-2, 0) -> Forfeit (that player will lose a dice)
        If a bet is not legit, the server will automatically replace it with Forfeit"""
        return self.get_safest_choice() # Change this line if you want another strategy

    # Natives (you can edit them, remove them and create new ones)

    def choice_is_legit(self, quantity, value, last_quantity, last_value):
        """Check if (quantity, value) is compatible with (last_quantity, last_value)"""
        if quantity==0 or quantity==-1:
            # Calza and Dudo are legit if it is not the first bet of the round.
            return last_quantity!=1 or last_value!=0
        elif quantity<0 or value not in (1, 2, 3, 4, 5, 6):
            # Bets on values that do not exist are not legit
            return False
        elif last_quantity==1 and last_value==0:
            # The first bet has no more constraint
            return True
        elif value==last_value:
            # same value (it can be on Paco)
            return quantity>last_quantity
        elif self.palifico:
            # Different values are not legit with Palifico
            return False
        elif value==1:
            # Normal -> Paco
            return quantity>=last_quantity/2
        elif last_value==1:
            # Paco -> Normal
            return quantity>=2*last_quantity+1
        else:
            # Normal -> Normal (different values)
            return quantity>last_quantity or (quantity==last_quantity and value>last_value)

    def factorial(self, n):
        """The factorial function"""
        x = 1
        for i in range(1, n+1):
            x *= i
        return x

    def n_chose_k(self, n, k):
        """The "n chose k" function"""
        return (self.factorial(n)//(self.factorial(k)*self.factorial(n-k)))

    def get_alive_players_number(self):
        """Return the number of remaining players (including this player)."""
        total = 0
        for dices_number in self.dices_number.values():
            if dices_number>0:
                total += 1
        return total

    def get_own_dices(self, value):
        """Returns the number of dices you currently own."""
        total = 0
        for dice in self.dices[self.pid]:
            if dice==value or ((not self.palifico) and dice==1):
                total += 1
        return total

    def get_total_other_dices(self):
        """ Return the total number of other players dices."""
        total = 0
        for pid in self.dices_number:
            if pid!=self.pid:
                total += self.dices_number[pid]
        return total

    def get_equal_proba(self, quantity, value):
        """"Returns the probability that (quantity, value) is *EXACT*.
        This probability takes this player's dices into account,
        but considers that all other dices are totally random.
        This grants a good first approximation of which bets are safe
        and which ones are risky."""
        own_dices = self.get_own_dices(value)
        total_other_dices = self.get_total_other_dices()
        goal = quantity - own_dices
        if self.palifico or value==1:
            p = 1/6
        else:
            p = 2/6
        proba = (p**goal)*((1-p)**(total_other_dices - goal))*self.n_chose_k(total_other_dices, goal)
        return proba

    def get_less_proba(self, quantity, value):
        """Return the probability that (quantity, value) is
        strictly less optimistic than the reality"""
        total = 0.0
        for i in range(quantity):
            total += self.get_equal_proba(i, value)
        return total

    def get_less_equal_proba(self, quantity, value):
        """Return the probability that (quantity, value) is
        correct or less optimistic than the reality"""
        return self.get_less_proba(quantity, value) + self.get_equal_proba(quantity, value)

    def get_greater_proba(self, quantity, value):
        """Return the probability that (quantity, value) is
        strictly more optimistic than the reality"""
        return 1.0 - self.get_less_equal_proba(quantity, value)

    def get_greater_equal_proba(self, quantity, value):
        """Return the probability that (quantity, value) is
        correct or more optimistic than the reality"""
        return 1.0 - self.get_less_proba(quantity, value)

    def get_safest_choice(self):
        """"Returns the probability that (quantity, value) is *EXACT*.
        This choice takes this player's dices into account,
        but considers that all other dices are totally random.
        This strategy is simple and quite fast to compute
        but it totally ignores the link between other player's bets and dices."""

        # raise_benefit determines how much succes chance the player is willing to lose
        # if it enable to choose a higher bet.
        # For instance, if raise_benefit is equal to 0.05
        # and the success chance of "2 Paco" is 0.50
        # and the success chance of "3 Paco" is 0.47",
        # it will chose "3 Paco" over "2" Paco"
        raise_benefit = 0.05

        # Success probability of Dudo
        total_dices = self.get_total_other_dices() + self.dices_number[self.pid]
        best_quantity = -1
        best_value = 0
        # Success probability of Calza
        if self.last_choice[0]!=1 or self.last_choice[1]!=0:
            best_proba = self.get_less_proba(*self.last_choice)
            proba = self.get_equal_proba(*self.last_choice)
            # If the player can win a dice,
            # Multiply his success "probability" by the number
            # of remaining other players 
            if self.dices_number[self.pid]<5:
                proba *= (self.get_alive_players_number()-1)
            if proba>=best_proba:
                best_proba = proba
                best_quantity = 0
        else:
            best_proba = 0
        # Standard maximum research on all legit choices
        # The only difference if that the player can reduce his success probability
        # if it enable it to choose a higher bet (see the comment above)
        for value in range(1, 7):
            for quantity in range(1, total_dices+1):
                if self.choice_is_legit(quantity, value, *self.last_choice):
                    proba = self.get_greater_equal_proba(quantity, value)
                    #print("Choice", quantity, value, "has a probability of success of", proba)
                    if proba + raise_benefit > best_proba:
                        best_proba = proba
                        best_quantity = quantity
                        best_value = value
                #else:
                    #print("Choice", quantity, value, "is not legit")
        if self.text_output:
            print(self.name, ": Betting", (best_quantity, best_value), "with a probability of succes of", best_proba)
        return (best_quantity, best_value)

