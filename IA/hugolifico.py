# default_IA.py by Victor Greiner
# This is an exemple of an IA file for superudo.
# You can copy and edit this if you want to create your own IA.
# The file name does not matter but the class name (IA) must remain the same
# and certain of it's methods must exist.

from random import randint, choice

class IA():

    # Interface methods 
    # These are the methods called by the server when a specific event occurs
    # or when an information about the IA is required
    # You can edit these methods but not remove them
    # (this would make the server crash)

    def __init__(self, player_number, pid, text_output):
        """This method is called upon game start."""
        self.player_number = player_number
        self.pid = pid
        self.text_output = text_output
        self.name = "Hugolifico"
        self.choices = {}
        self.dices_number = {}
        self.dices = {}
        self.palifico = False
        self.last_choice = (1, 0)
        for i in range(player_number):
            self.choices[i] = None
            self.dices_number[i] = 5
            self.dices[i] = []

    def get_name(self):
        """Return the name of the player"""
        return self.name

    def new_round(self):
        """This method is called upon each round start."""
        self.palifico = False
        self.last_choice = (1, 0)
        for pid in self.choices:
            self.choices[pid] = (1, 0)
            self.dices[pid] = []

    def set_dices(self, dices):
        """This method is called right after the round start
        to let the player know what his dices are."""
        self.dices[self.pid] = dices

    def set_player_dices(self, pid, dices):
        """This method is called at the end of each round
        to let the player know what other player's dices are."""
        self.dices[pid] = dices

    def set_player_choice(self, pid, choice):
        """This method is called whenever a player makes a bet."""
        self.choices[pid] =  choice
        self.last_choice = choice[:]

    def remove_player_dice(self, pid):
        """This method is called whenever a player loses a dice."""
        self.dices_number[pid] -= 1

    def add_player_dice(self, pid):
        """This method is called whenever a player gains a dice."""
        self.dices_number[pid] += 1

    def set_palifico(self):
        """This method is called right after the beginning of a round with Palifico."""
        self.palifico = True

    def play(self):
        """This method is called when this player has to make a bet.
        It must return a couble of int that represent a bet.
        Examples :
            (-1, 0) -> Dudo
            (0, 0) -> Calza
            (2, 1) -> 2 Paco
            (3, 5) -> 3 Dices with a value of 5
            (-2, 0) -> Forfeit (that player will lose a dice)
        If a bet is not legit, the server will automatically replace it with Forfeit"""
        return (-1,0) # Change this line if you want another strategy

    # Natives (you can edit them, remove them and create new ones)

    def choice_is_legit(self, quantity, value, last_quantity, last_value):
        """Check if (quantity, value) is compatible with (last_quantity, last_value)"""
        if quantity==0 or quantity==-1:
            # Calza and Dudo are legit if it is not the first bet of the round.
            return last_quantity!=1 or last_value!=0
        elif quantity<0 or value not in (1, 2, 3, 4, 5, 6):
            # Bets on values that do not exist are not legit
            return False
        elif last_quantity==1 and last_value==0:
            # The first bet has no more constraint
            return True
        elif value==last_value:
            # same value (it can be on Paco)
            return quantity>last_quantity
        elif self.palifico:
            # Different values are not legit with Palifico
            return False
        elif value==1:
            # Normal -> Paco
            return quantity>=last_quantity/2
        elif last_value==1:
            # Paco -> Normal
            return quantity>=2*last_quantity+1
        else:
            # Normal -> Normal (different values)
            return quantity>last_quantity or (quantity==last_quantity and value>last_value) 
